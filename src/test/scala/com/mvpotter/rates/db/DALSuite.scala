/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 02/03/15
 * Time: 22:58
 */
package com.mvpotter.rates.db

import com.mvpotter.rates.model.Currency
import org.joda.time.DateTime
import org.scalatest.{BeforeAndAfter, FunSuite}

import scala.slick.driver.H2Driver.simple._
import scala.slick.jdbc.JdbcBackend.{Database, Session}
import scala.slick.jdbc.meta.MTable

class DALSuite extends FunSuite with BeforeAndAfter with TestDB {
  val date = DateTime.now
  val ratesList = List(com.mvpotter.rates.model.Rate(com.mvpotter.rates.model.Bank("Test bank"), Currency.RUB, Currency.USD, 6.0, 6.5, date))

  test("Creating the Schema works") {
    db.withSession { implicit session =>
      dal.create

      val tables = MTable.getTables.list

      assert(tables.size == 4)
      assert(tables.count(_.name.name.equalsIgnoreCase("banks")) == 1)
      assert(tables.count(_.name.name.equalsIgnoreCase("locations")) == 1)
      assert(tables.count(_.name.name.equalsIgnoreCase("phones")) == 1)
      assert(tables.count(_.name.name.equalsIgnoreCase("rates")) == 1)
    }
  }

  test("Inserting a Rate works") {
    db.withSession { implicit session =>
      dal.create

      saveRates(ratesList)
      assert(dal.rates.length.run == 1)
      assert(dal.banks.length.run == 1)
    }
  }

  test("Repeated inserting of a Rate is ignored") {
    db.withSession { implicit session =>
      dal.create

      saveRates(ratesList)
      saveRates(ratesList)
      assert(dal.rates.length.run == 1)
      assert(dal.banks.length.run == 1)
    }
  }

  def saveRates(ratesList: List[com.mvpotter.rates.model.Rate]): Unit = {
    db.withSession { implicit session =>
      ratesList.foreach { r =>
        dal.insert(Rate(bank = Bank(name = r.bank.name),
          sourceCurrency = r.sourceCurrency.toString, destCurrency = r.destCurrency.toString,
          buyRate = r.buyRate, sellRate = r.sellRate, date = r.date))
      }
    }
  }

}
