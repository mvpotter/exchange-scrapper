/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 01/03/15
 * Time: 13:16
 */
package com.mvpotter.rates.model

case class Bank(name: String, branches: Option[Set[Location]])

object Bank {

  def apply(name: String): Bank = {
    Bank(name, None)
  }

}


