/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 01/03/15
 * Time: 13:16
 */
package com.mvpotter.rates.model

import org.joda.time.DateTime

object Currency extends Enumeration {
  type Currency = Value
  val RUB, USD, EUR = Value
}

case class Rate(bank: Bank,
                sourceCurrency: Currency.Value,
                destCurrency: Currency.Value,
                buyRate: Double,
                sellRate: Double,
                date: DateTime)


object Rate {

  def apply(bank: Bank, sourceCurrency: Currency.Value, destCurrency: Currency.Value, exchangeRate: Double, date: DateTime): Rate = {
    Rate(bank, sourceCurrency, destCurrency, exchangeRate, exchangeRate, date)
  }

}
