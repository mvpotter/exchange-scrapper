/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 01/03/15
 * Time: 13:16
 */
package com.mvpotter.rates.model

case class Location(distinct: String, address: String, station: String, phones: Option[Set[String]])
