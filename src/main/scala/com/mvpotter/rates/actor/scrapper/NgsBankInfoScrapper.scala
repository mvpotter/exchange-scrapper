/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 01/03/15
 * Time: 17:26
 */
package com.mvpotter.rates.actor.scrapper

import akka.actor.{ActorRef, Actor, ActorLogging, PoisonPill}
import com.mvpotter.rates.model.{Bank, Location}
import org.jsoup.nodes.{Document, Element}
import org.jsoup.select.Elements
import org.jsoup.{Connection, Jsoup}
import com.newrelic.api.agent.Trace

import scala.collection.JavaConversions._

object ScrapBankProtocol {
  case class ScrapBankInfo(receiver: ActorRef, bankName: String, bankUrl: String)
  case class BankInfo(bank: Bank)
}

class NgsBankInfoScrapper extends Actor with ActorLogging {
  import com.mvpotter.rates.actor.scrapper.ScrapBankProtocol._

  override def receive: Receive = {
    case ScrapBankInfo(receiver, bankName, bankUrl) =>
      scrapBankInfo(receiver, bankName, bankUrl)
      context.stop(self)
  }

  @Trace
  def scrapBankInfo(receiver: ActorRef, name: String, bankUrl: String): Unit = {
    val connection = Jsoup.connect(bankUrl).timeout(15000)
    val response: Connection.Response = connection.execute()
    if (response.statusCode() != 200) {
      log.info(s"response status = ${response.statusCode()}")
      return
    }
    val doc: Document = response.parse()
    def text(column: Element): String = {
      val links: Elements = column.select("a")
      if (links.isEmpty) {
        column.text()
      } else {
        links.get(0).text()
      }
    }
    val tableRows = doc.select("table.currency > tbody > tr")
    if (tableRows.isEmpty) {
      log.info("There is no bank info on page")
      return
    }
    val locations: scala.collection.mutable.Set[Location] = scala.collection.mutable.Set[Location]()
    tableRows.toList.foreach { row =>
      val cols: Elements = row.select("td")
      val phones = cols.get(4).text().split(",").map(_.trim)
      val phonesSet: Option[Set[String]] = phones match {
        case _ if phones.isEmpty => None
        case _ => Some(phones.toSet)
      }
      locations += Location(text(cols.get(1)), text(cols.get(2)), text(cols.get(3)), phonesSet)

    }
    val locationsSet: Option[Set[Location]] = locations match {
      case _ if locations.isEmpty => None
      case _ => Some(locations.toSet)
    }

    receiver ! BankInfo(Bank(name, locationsSet))
  }

}
