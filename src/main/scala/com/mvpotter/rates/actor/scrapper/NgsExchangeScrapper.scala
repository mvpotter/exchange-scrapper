package com.mvpotter.rates.actor.scrapper

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.mvpotter.rates.model.{Bank, Rate}
import com.newrelic.api.agent.Trace
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import org.joda.time.{DateTime, DateTimeZone}
import org.jsoup.nodes.Document
import org.jsoup.select.Elements
import org.jsoup.{Connection, Jsoup}

import scala.collection.JavaConversions._

object ScrapRatesProtocol {
  case object ScrapRates
  case class Rates(rates: List[Rate])
}

class NgsExchangeScrapper extends Actor with ActorLogging {
  import com.mvpotter.rates.actor.scrapper.ScrapBankProtocol._
  import com.mvpotter.rates.actor.scrapper.ScrapRatesProtocol._

  override def receive: Receive = {
    case ScrapRates =>
      scrap(sender)
  }

  val ngsBusinessUrl = "http://business.ngs.ru"
  val ngsBusinessCurrencyUrl = "http://business.ngs.ru/currency"
  val fmt: DateTimeFormatter = DateTimeFormat.forPattern("dd.MM HH:mm")

  import com.mvpotter.rates.model.Currency._

  @Trace(dispatcher = true)
  def scrap(receiver: ActorRef): Unit = {
    log.info("scrap start")
    val connection = Jsoup.connect(ngsBusinessCurrencyUrl).timeout(15000)
    val response: Connection.Response = connection.execute()
    if (response.statusCode() != 200) {
      log.info(s"response status = ${response.statusCode()}")
      return
    }
    val doc: Document = response.parse()
    val centralBankCurrencies: Elements = doc.select(".bus-currency-box__informer-val")
    val date = DateTime.now.withZone(DateTimeZone.forID("Asia/Novosibirsk"))
    val exchangeRates: scala.collection.mutable.MutableList[Rate] = scala.collection.mutable.MutableList[Rate](
                                          Rate(Bank("ЦБ"), RUB, USD, centralBankCurrencies.get(0).text().toDouble, date),
                                          Rate(Bank("ЦБ"), RUB, EUR, centralBankCurrencies.get(1).text().toDouble, date)
                                        )

    doc.select("table.currency > tbody > tr").toList.foreach { row =>
      val cols: Elements = row.select("td > a")
      val bankName = cols.get(0).text()
      val bankUrl: String = ngsBusinessUrl + cols.get(0).attr("href")
      val bankId = bankUrl.substring(bankUrl.indexOf("?") + 1)
      context.child(bankId) match {
        case Some(bankScrapper) => bankScrapper ! ScrapBankInfo(receiver, bankName, bankUrl)
        case None => context.actorOf(Props[NgsBankInfoScrapper], bankId) ! ScrapBankInfo(receiver, bankName, bankUrl)
      }
      val infoDate: DateTime = fmt.parseDateTime(cols.get(5).text()).withYear(date.year.get())
      exchangeRates += Rate(Bank(bankName), RUB, USD, cols.get(1).text().toDouble, cols.get(2).text().toDouble, infoDate)
      exchangeRates += Rate(Bank(bankName), RUB, EUR, cols.get(3).text().toDouble, cols.get(4).text().toDouble, infoDate)
    }

    log.info("scrap finish")
    receiver ! Rates(exchangeRates.toList)
  }
  
}

