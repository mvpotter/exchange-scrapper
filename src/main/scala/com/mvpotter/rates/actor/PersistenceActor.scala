/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 02/03/15
 * Time: 23:04
 */
package com.mvpotter.rates.actor

import akka.actor.{Actor, ActorLogging}
import com.mvpotter.rates.db.{Bank, ProductionDB, Rate}

class PersistenceActor extends Actor with ActorLogging with ProductionDB {
  import com.mvpotter.rates.actor.scrapper.ScrapBankProtocol._
  import com.mvpotter.rates.actor.scrapper.ScrapRatesProtocol._

  def receive = {
    case Rates(rates) => saveRates(rates); log.debug("\n" + (rates mkString "\n"))
    case BankInfo(bank) => log.debug(bank.toString)
  }

  def saveRates(ratesList: List[com.mvpotter.rates.model.Rate]): Unit = {
    db.withSession { implicit session =>
      ratesList.foreach { r =>
        dal.insert(Rate(bank = Bank(name = r.bank.name),
                        sourceCurrency = r.sourceCurrency.toString, destCurrency = r.destCurrency.toString,
                        buyRate = r.buyRate, sellRate = r.sellRate, date = r.date))
      }
    }
  }

}
