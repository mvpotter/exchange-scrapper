/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 01/03/15
 * Time: 20:53
 */
package com.mvpotter.rates.db

import com.github.tototoshi.slick.JdbcJodaSupport._
import org.joda.time.DateTime

case class Rate(id: Option[Int] = None,
                bank: Bank,
                sourceCurrency: String,
                destCurrency: String,
                buyRate: Double,
                sellRate: Double,
                date: DateTime)

trait RateComponent  { this: DriverComponent with BankComponent =>
  import driver.simple._

  class Rates(tag: Tag) extends Table[(Option[Int], Int, String, String, Double, Double, DateTime)](tag, "rates") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def bankId = column[Int]("bank_id")
    def sourceCurrency = column[String]("source_currency")
    def destCurrency = column[String]("dert_currency")
    def buyRate = column[Double]("buy_rate")
    def sellRate = column[Double]("sell_rate")
    def date = column[DateTime]("date")
    def * = (id.?, bankId, sourceCurrency, destCurrency, buyRate, sellRate, date)
  }

  val rates = TableQuery[Rates]

  private val ratesAutoInc = rates.map(r => (r.bankId, r.sourceCurrency, r.destCurrency, r.buyRate, r.sellRate, r.date)) returning rates.map(_.id)

  def insert(rate: Rate)(implicit session: Session): Rate = {
    val bank =
      if (rate.bank.id.isEmpty) insert(rate.bank)
      else rate.bank
    val storedRate =
      rates.filter { storedRate =>
        storedRate.bankId === bank.id.get &&
        storedRate.sourceCurrency === rate.sourceCurrency &&
        storedRate.destCurrency === rate.destCurrency &&
        storedRate.sellRate === rate.sellRate &&
        storedRate.buyRate === rate.buyRate &&
        storedRate.date === rate.date
      }.firstOption
    storedRate match {
      case Some(r) => Rate(r._1, bank, r._3, r._4, r._5, r._6, r._7)
      case None =>
        val id = ratesAutoInc.insert(bank.id.get, rate.sourceCurrency, rate.destCurrency, rate.buyRate, rate.sellRate, rate.date)
        rate.copy(bank = bank, id = Some(id))
    }
  }

  def lastRates(currency: String)(implicit session: Session) = {
    val todayStart = DateTime.now().withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)
    val lastDateQuery = rates.filter(_.date >= todayStart).groupBy(_.bankId).map {
      case (bankId, r) => bankId -> r.map(_.date).max
    }

    val lastRates = for {
      (bankId, date) <- lastDateQuery
      rate <- rates if rate.bankId === bankId && rate.date === date && rate.destCurrency === currency
      bank <- banks if bank.id === bankId
    } yield (rate, bank)

    lastRates.sortBy(_._1.date.asc).list.map(
      (item: ((Option[Int], Int, String, String, Double, Double, DateTime), Bank)) =>
        Rate(item._1._1, item._2, item._1._3, item._1._4, item._1._5, item._1._6, item._1._7)
    )
  }

  // TODO: move date_trunc to separate class
  // PgSQL specific function
  val day = "day"
  val date_trunc = SimpleFunction.binary[String, DateTime, DateTime]("date_trunc")
  def dateTruncate(precision: String, date: Column[DateTime]): Column[DateTime] = date_trunc(precision, date)

  def lastRates(bankName: String, currency: String, since: DateTime)(implicit session: Session) = {
    val lastTimeQuery = for {
      bank <- banks if bank.name === bankName
      (_, maxTime) <- rates.filter { rate =>
        rate.bankId === bank.id &&
        rate.date >= since
      }.groupBy(rate => dateTruncate(day, rate.date)).map {
        case (d, rate) => day -> rate.map(_.date).max
      }
    } yield maxTime

    val lastRates = for {
      bank <- banks if bank.name === bankName
      maxTime <- lastTimeQuery
      rate <- rates if rate.date === maxTime && rate.destCurrency === currency && rate.bankId === bank.id
    } yield (rate, bank)

    lastRates.sortBy(_._1.date.asc).list.map(
      (item: ((Option[Int], Int, String, String, Double, Double, DateTime), Bank)) =>
        Rate(item._1._1, item._2, item._1._3, item._1._4, item._1._5, item._1._6, item._1._7)
    )
  }

}
