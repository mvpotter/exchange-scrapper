package com.mvpotter.rates.db

import scala.slick.driver.JdbcProfile

trait DriverComponent {
  val driver: JdbcProfile
}
