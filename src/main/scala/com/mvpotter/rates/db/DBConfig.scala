package com.mvpotter.rates.db

import com.mchange.v2.c3p0.ComboPooledDataSource
import com.mvpotter.rates.config.Settings

import scala.slick.driver.PostgresDriver
import scala.slick.jdbc.JdbcBackend._
import scala.slick.jdbc.meta.MTable

trait DBConfig {

}

trait TestDB extends DBConfig {
  val db = {
    val ds = new ComboPooledDataSource
    ds.setDriverClass("org.h2.Driver")
    ds.setJdbcUrl("jdbc:h2:mem:rates_test")
    ds.setInitialPoolSize(5)
    ds.setMinPoolSize(5)
    ds.setAcquireIncrement(5)
    ds.setMaxPoolSize(20)
    Database.forDataSource(ds)
  }
  val dal: DAL = new DAL(PostgresDriver)
  db.withSession { implicit session =>
    if (MTable.getTables("rates").list.isEmpty) {
      dal.create
    }
  }
}

trait ProductionDB extends DBConfig {
  val db = {
    val ds = new ComboPooledDataSource
    ds.setDriverClass(Settings.driver)
    ds.setJdbcUrl(Settings.connect)
    ds.setUser(Settings.username)
    ds.setPassword(Settings.password)
    ds.setInitialPoolSize(5)
    ds.setMinPoolSize(5)
    ds.setAcquireIncrement(5)
    ds.setMaxPoolSize(20)
    Database.forDataSource(ds)
  }
  val dal: DAL = new DAL(PostgresDriver)
  db.withSession { implicit session =>
    if (MTable.getTables("rates").list.isEmpty) {
      dal.create
    }
  }
}