/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 01/03/15
 * Time: 20:45
 */
package com.mvpotter.rates.db

case class Branch(id: Option[Int] = None, bankId: Int, distinct: String, address: String, station: String)

trait BranchComponent  { this: DriverComponent =>
  import driver.simple._

  class Branches(tag: Tag) extends Table[Branch](tag, "locations") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def bankId = column[Int]("bank_id")
    def distinct = column[String]("distinct")
    def address = column[String]("address")
    def station = column[String]("station")
    override def * = (id.?, bankId, distinct, address, station) <> (Branch.tupled, Branch.unapply)
  }

  val branches = TableQuery[Branches]

  private val branchesAutoInc = branches returning branches.map(_.id) into {
    case (branch, id) => branch.copy(id = Some(id))
  }

  def insert(branch: Branch)(implicit session: Session): Branch =
    branchesAutoInc.insert(branch)

}