/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 01/03/15
 * Time: 23:55
 */
package com.mvpotter.rates.db

case class Phone(id: Option[Int] = None, locationId: Int, number: String)

trait PhoneComponent  { this: DriverComponent =>
  import driver.simple._

  class Phones(tag: Tag) extends Table[Phone](tag, "phones") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def locationId = column[Int]("location_id")
    def number = column[String]("number")
    override def * = (id.?, locationId, number) <> (Phone.tupled, Phone.unapply)
  }

  val phones = TableQuery[Phones]

  private val phonesAutoInc = phones returning phones.map(_.id) into {
    case (phone, id) => phone.copy(id = Some(id))
  }

  def insert(phone: Phone)(implicit session: Session): Phone =
    phonesAutoInc.insert(phone)

}
