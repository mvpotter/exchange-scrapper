package com.mvpotter.rates.db

import scala.slick.driver.JdbcProfile

class DAL(val driver: JdbcProfile) extends DriverComponent with BankComponent
                                                           with BranchComponent
                                                           with PhoneComponent
                                                           with RateComponent {
  import driver.simple._

  def create(implicit session: Session) = (banks.ddl ++ branches.ddl ++ phones.ddl ++ rates.ddl).create
}
