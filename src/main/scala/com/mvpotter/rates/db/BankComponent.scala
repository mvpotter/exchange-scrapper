/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 01/03/15
 * Time: 20:39
 */
package com.mvpotter.rates.db

case class Bank(id: Option[Int] = None, name: String)

trait BankComponent  { this: DriverComponent =>
  import driver.simple._

  class Banks(tag: Tag) extends Table[Bank](tag, "banks") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    override def * = (id.?, name) <> (Bank.tupled, Bank.unapply)
  }

  val banks = TableQuery[Banks]

  private val banksAutoInc = banks returning banks.map(_.id) into {
    case (bank, id) => bank.copy(id = Some(id))
  }

  def insert(bank: Bank)(implicit session: Session): Bank = {
    banks.filter(_.name === bank.name).firstOption match {
      case Some(b) => b
      case None => banksAutoInc.insert(bank)
    }
  }

}





