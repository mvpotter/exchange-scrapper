package com.mvpotter.rates.config

import java.net.URI
import com.typesafe.config.ConfigFactory

/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 03/03/15
 * Time: 22:07
 */
object Settings {
  private val config = ConfigFactory.load()
  private val dbConfig = config.getConfig("database")
  private val dbUri = new URI(System.getenv("DATABASE_URL"))
  private val credentials = dbUri.getUserInfo.split(":")

  def driver = dbConfig.getString("driver")
  def username = credentials(0)
  def password = if (credentials.length > 1) credentials(1) else ""
  def connect = s"jdbc:postgresql://${dbUri.getHost}:${dbUri.getPort}${dbUri.getPath}"

}
