package com.mvpotter.rates.web

import java.util

import akka.actor.Actor
import akka.event.Logging.InfoLevel
import com.mvpotter.rates.db.{RateComponent, Rate, Bank, ProductionDB}
import com.newrelic.api.agent._
import spray.http.MediaTypes._
import spray.http._
import spray.json._
import spray.routing._
import spray.routing.directives.LogEntry
import org.joda.time.DateTime

object RatesJsonProtocol extends DefaultJsonProtocol {
  implicit val bankFormat = jsonFormat2(Bank)
  implicit object DateJsonFormat extends RootJsonFormat[DateTime] {
    override def write(date: DateTime): JsValue = {
      JsString(date.toString("HH:mm dd.MM.yyyy"))
    }

    override def read(json: JsValue): DateTime = json match {
      case JsNumber(millis) => new DateTime(millis)
      case _ => deserializationError("Date in milliseconds expected")
    }
  }
  implicit object RateJsonFormat extends RootJsonFormat[Rate] {
    override def write(rate: Rate): JsValue = JsObject(
      "id" -> rate.id.map(JsNumber(_)).getOrElse(JsNull),
      "bank" -> rate.bank.toJson,
      "source" -> JsString(rate.sourceCurrency),
      "dest" -> JsString(rate.destCurrency),
      "buy" -> JsNumber(rate.buyRate),
      "sell" -> JsNumber(rate.sellRate),
      "date" -> rate.date.toJson
    )

    override def read(value: JsValue): Rate = {
      value.asJsObject.getFields("id", "bank", "source", "dest", "buy", "sell", "date") match {
        case Seq(JsNumber(id), JsObject(bank), JsString(sourceCurrency), JsString(destCurrency), JsNumber(buyRate), JsNumber(sellRate), JsString(date)) =>
          Rate(Some(id.toInt), bankFormat.read(JsObject(bank)), sourceCurrency, destCurrency, buyRate.toDouble, sellRate.toDouble, DateJsonFormat.read(JsString(date)))
        case _ => deserializationError("Rate expected")
      }
    }
  }
}

class RatesServiceActor extends Actor with RatesService with StaticService {

  implicit val ratesRejectionHandler = RejectionHandler {
    case Nil =>
      respondWithStatus(404) {
        getFromResource("frontend/error/404.html")
      }
  }

  // the HttpService trait defines only one abstract member, which
  // connects the services environment to the enclosing actor or test
  def actorRefFactory = context

  def getHttpHeader(name: String, httpMessage: HttpMessage): String = {
    httpMessage.headers.collectFirst({case header: HttpHeader if header.name == name => header.value}).orNull
  }

  @Trace(dispatcher = true)
  def requestMethodAndResponseStatusAsInfo(req: HttpRequest): Any => Option[LogEntry] = {
    case res: HttpResponse =>
      NewRelic.setRequestAndResponse(
        new Request {
          override def getRequestURI: String = req.uri.toString()
          override def getRemoteUser: String = null
          override def getParameterNames: util.Enumeration[_] = null
          override def getAttribute(name: String): AnyRef = null
          override def getParameterValues(name: String): Array[String] = null
          override def getCookieValue(name: String): String = req.cookies.collectFirst({case cookie: HttpCookie if cookie.name == name => cookie.value}).orNull
          override def getHeaderType: HeaderType = HeaderType.HTTP
          override def getHeader(name: String): String = getHttpHeader(name, req)
        },
        new Response {
          override def getStatus: Int = res.status.intValue
          override def getStatusMessage: String = res.status.defaultMessage
          override def getContentType: String = getHttpHeader("Content-Type", res)
          override def getHeaderType: HeaderType = HeaderType.HTTP
          override def setHeader(name: String, value: String): Unit = {}
        }
      )
      Some(LogEntry(req.method + ":" + req.uri + ":" + res.message.status, InfoLevel))
    case _ => None // other kind of responses
  }

  def routeWithLogging = logRequestResponse(requestMethodAndResponseStatusAsInfo _)(ratesRoutes ~ staticRoutes)

  // this actor only runs our route, but you could add
  // other things here, like request stream processing
  // or timeout handling
  def receive = runRoute(routeWithLogging)
}

// this trait defines our service behavior independently from the service actor
trait RatesService extends HttpService with ProductionDB {

  import RatesJsonProtocol._
  import spray.httpx.SprayJsonSupport._
  import com.github.tototoshi.slick.JdbcJodaSupport._
  import dal.driver.simple._

  val ratesRoutes =
    pathPrefix("api") {
      path("banks") {
        get {
          db.withSession { implicit session =>
            respondWithMediaType(`application/json`) {
              complete(dal.banks.list)
            }
          }
        }
      } ~
      pathPrefix("rates") {
        get {
          path("week") {
            respondWithMediaType(`application/json`) {
              val lastWeek = DateTime.now.minusWeeks(1)
              db.withSession { implicit session =>
                complete(dal.lastRates("ЦБ", "EUR", lastWeek))
              }
            }
          } ~
          path("month") {
            respondWithMediaType(`application/json`) {
              val lastMonth = DateTime.now.minusMonths(1)
              db.withSession { implicit session =>
                complete(dal.lastRates("ЦБ", "EUR", lastMonth))
              }
            }
          } ~
          path("quarter") {
            respondWithMediaType(`application/json`) {
              val lastQuarter = DateTime.now.minusMonths(3)
              db.withSession { implicit session =>
                complete(dal.lastRates("ЦБ", "EUR", lastQuarter))
              }
            }
          } ~
          pathEnd {
            parameters('currency) { currency =>
              respondWithMediaType(`application/json`) {
                db.withSession { implicit session =>
                  complete(dal.lastRates(currency))
                }
              }
            }
          }
        }
      }
    }
}

trait StaticService extends HttpService {
  val staticRoutes = {
    pathPrefix(!"api" ~ !"js" ~ !"favicon.ico") {
      getFromResource("frontend/index.html")
    } ~ {
      getFromResourceDirectory("frontend")
    }
  }
}