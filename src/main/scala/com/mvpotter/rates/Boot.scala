package com.mvpotter.rates

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import akka.pattern.ask
import akka.util.Timeout
import com.mvpotter.rates.actor.PersistenceActor
import com.mvpotter.rates.actor.scrapper.NgsExchangeScrapper
import com.mvpotter.rates.web.RatesServiceActor
import org.slf4j.LoggerFactory
import spray.can.Http

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.util.Properties

/*
 * Created with IntelliJ IDEA.
 * User: michaelpotter
 * Date: 27/02/15
 * Time: 23:43
 */
object Boot extends App {

  val log = LoggerFactory.getLogger(Boot.getClass)
  implicit val system = ActorSystem("exchange")

  // create and start our service actor
  val service = system.actorOf(Props[RatesServiceActor], "rates-service")
  implicit val timeout = Timeout(1.minute)
  // for Heroku compatibility
  val port = Properties.envOrElse("PORT", "8080").toInt
  log.info(s"port=$port")
  // start a new HTTP server on port 8080 with our service actor as the handler
  val boundFuture = IO(Http) ? Http.Bind(service, interface = "0.0.0.0", port = port)

  boundFuture.onSuccess { case _ =>
    import com.mvpotter.rates.actor.scrapper.ScrapRatesProtocol._
    // schedule scrappers
    val scrapper = system.actorOf(Props[NgsExchangeScrapper], "scrapper")
    val exchangePrinter = system.actorOf(Props[PersistenceActor], "persistence")
    log.info("schedule scrapping task")
    system.scheduler.schedule(10.seconds, 1.hour, scrapper, ScrapRates)(system.dispatcher, exchangePrinter)
  }

}
