var RatesBox = React.createClass({
    loadCommentsFromServer: function() {
        $.ajax({
            url: '/api/rates',
            dataType: 'json',
            type: 'get',
            data: {currency: 'EUR'},
            success: function(rates) {
                this.setState({rates: rates});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    getInitialState: function() {
        return {rates: []};
    },
    componentDidMount: function() {
        this.loadCommentsFromServer();
    },
    render: function() {
        return (
            <div className="commentBox">
                <RatesTable rates={this.state.rates}/>
            </div>
        );
    }
});

var RatesTable = React.createClass({
    render: function() {
        var ratesNodes = this.props.rates.map(function (rate) {
            return (
                <RateRow bank={rate.bank.name} currency={rate.dest} buy={rate.buy} sell={rate.sell} date={rate.date}/>
            );
        });
        return (
            <table className="table">
                <thead>
                    <th>Банк</th>
                    <th>Валюта</th>
                    <th>Покупка</th>
                    <th>Продажа</th>
                    <th>Последнее обновление</th>
                </thead>
                <tbody>
		    {ratesNodes}
		</tbody>
            </table>
        );
    }
});

var RateRow = React.createClass({
    render: function() {
        return (
            <tr>
                <td>
                    {this.props.bank}
                </td>
                <td>
                    {this.props.currency}
                </td>
                <td>
                    {parseFloat(Math.round(this.props.buy * 100) / 100).toFixed(2)}
                </td>
                <td>
                    {parseFloat(Math.round(this.props.sell * 100) / 100).toFixed(2)}
                </td>
                <td>
                    {this.props.date}
                </td>
            </tr>
        );
    }
});

var RateControls = React.createClass({
    getInitialState: function() {
        return {period: this.props.period};
    },
    componentDidMount: function() {
        this.handlePeriodChange(this.state.period);
    },
    handlePeriodChange: function(period) {
        if (this.state.period != period) {
            this.setState({period: period});
            this.props.onPeriodChange(period);
        }
    },
    render: function() {
        return (
            <div className="btn-group pull-right" role="group" aria-label="...">
                <button type="button" className="btn btn-default" onClick={this.handlePeriodChange.bind(null, "week")}>Week</button>
                <button type="button" className="btn btn-default" onClick={this.handlePeriodChange.bind(null, "month")}>Month</button>
                <button type="button" className="btn btn-default" onClick={this.handlePeriodChange.bind(null, "quarter")}>Quarter</button>
            </div>
        );
    }

});

function data(labels, values) {
    return {
        labels: labels,
        datasets: [
            {
                label: "My First dataset",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: values
            }
        ]
    }
}

Chart.defaults.global.responsive = true;
var LineChart = Chart.React['Line'];
var RatesChart = React.createClass({
    loadRatesFromServer: function(period) {
        $.ajax({
            url: '/api/rates/' + period,
            dataType: 'json',
            success: function(rates) {
                var labels = [];
                var values = [];
                rates.forEach(function(rate) {
                    labels.push(rate.date.split(" ")[1]);
                    values.push(parseFloat(Math.round(rate.sell * 100) / 100).toFixed(2));
                });
                this.setState({period: this.state.period, data: data(labels, values)});
                // it is necessary to set state again, otherwise chart is not redrawn
                this.setState(this.state.data);
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    getInitialState: function() {
        return {period: "month", data: data([], [])};
    },
    componentDidMount: function() {
        this.loadRatesFromServer(this.state.period);
    },
    handlePeriodChange: function(period) {
        if (this.state.period != period) {
            this.setState({period: period, data: this.state.data});
            this.loadRatesFromServer(period);
        }
    },
    render: function() {
        return (
            <div>
                <RateControls period={this.state.period} onPeriodChange={this.handlePeriodChange} />
                <LineChart data={this.state.data} />
            </div>
        );
    }
});


