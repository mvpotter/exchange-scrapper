var Router = ReactRouter;

var DefaultRoute = Router.DefaultRoute;
var Link = Router.Link;
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;

var Rates = React.createClass({
    render: function () {
        return (
            <div>
                <nav className="navbar navbar-default navbar-fixed-top">
                    <div className="container">
                        <div className="navbar-header">
                            <a className="navbar-brand" href="/">Курсы валют</a>
                        </div>

                        <div className="collapse navbar-collapse">
                            <ul className="nav navbar-nav">
                                <li><Link to="rates">Курсы</Link></li>
                                <li><Link to="graphs">Графики</Link></li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <RouteHandler/>
            </div>
        );
    }
});

var routes = (
    <Route name="rates" path="/" handler={Rates}>
        <Route name="graphs" handler={RatesChart}/>
        <DefaultRoute handler={RatesBox}/>
    </Route>
);

Router.run(routes, Router.HistoryLocation, function (Handler) {
    React.render(<Handler/>, document.getElementById('content'));
});