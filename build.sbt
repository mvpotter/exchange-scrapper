packageArchetype.java_application

name := "exchange-scrapper"

version := "1.0"

scalaVersion := "2.11.5"

libraryDependencies ++= {
  val jsoupVersion = "1.8.1"
  val nscalaTimeVersion = "1.8.0"
  val jodaMapperVersion = "1.2.0"
  val slickVersion = "2.1.0"
  val logbackVersion = "1.1.2"
  val akkaVersion = "2.3.9"
  val sprayVersion = "1.3.2"
  val sprayJsonVersion = "1.3.1"
  val h2Version = "1.3.175"
  val postgresqlVersion = "9.1-901-1.jdbc4"
  val c3p0Version = "0.9.5"
  val newrelicVersion = "3.14.0"
  val scalaTestVersion = "2.2.4"
  Seq(
    "org.jsoup"               % "jsoup"              % jsoupVersion,
    "com.github.nscala-time" %% "nscala-time"        % nscalaTimeVersion,
    "com.github.tototoshi"   %% "slick-joda-mapper"  % jodaMapperVersion,
    "ch.qos.logback"          % "logback-classic"    % logbackVersion,
    "com.typesafe.slick"     %% "slick"              % slickVersion,
    "com.typesafe.akka"      %% "akka-actor"         % akkaVersion,
    "com.typesafe.akka"      %% "akka-slf4j"         % akkaVersion,
    "io.spray"               %% "spray-can"          % sprayVersion,
    "io.spray"               %% "spray-routing"      % sprayVersion,
    "io.spray"               %% "spray-json"         % sprayJsonVersion,
    "com.h2database"          % "h2"                 % h2Version,
    "com.mchange"             % "c3p0"               % c3p0Version,
    "postgresql"              % "postgresql"         % postgresqlVersion,
    "com.newrelic.agent.java" % "newrelic-api"       % newrelicVersion,
    "com.newrelic.agent.java" % "newrelic-agent"     % newrelicVersion,
    "io.spray"               %% "spray-testkit"      % sprayVersion       % "test",
    "com.typesafe.akka"      %% "akka-testkit"       % akkaVersion        % "test",
    "org.scalatest"          %% "scalatest"          % scalaTestVersion   % "test"
  )
}